## To use this project?

1. Fork/Clone
1. Install dependencies - `npm install`
1. Install MySql DB locally
   `brew update`
   `brew install mysql`
1. Now start the mysql   
   `brew services start mysql`
1. Create a local mysql databases - `petshop`
1. Update the user/password in `knexfile.js` to use your prefered value. Default user is `root` and password is `password`
1. Run the following Migrate command in the terminal - `knex migrate:latest --env development`
1. Run the following to Seed the db - `knex seed:run --env development`