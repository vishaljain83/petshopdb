
exports.seed = (knex, Promise) => {
  // Deletes ALL existing entries
  return knex('pets').del()
    .then(function () {
      // Inserts seed entries
      return knex('pets').insert([
        {
          name: 'Ghost', 
          birthday: '2011-04-17'
        },
        {
          name: 'Drogon', 
          birthday: '2011-06-19'
        },
        {
          name: 'Anger', 
          birthday: '2011-05-10'
        }
      ]);
    });
};
