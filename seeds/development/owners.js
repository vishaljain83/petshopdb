
exports.seed = (knex, Promise) => {
  // Deletes ALL existing entries
  return knex('owners').del()
    .then(() => {
      // Inserts seed entries
      return knex('owners').insert([
        {
          first_name: 'Jhon', 
          last_name: 'Snow',
          city: 'Witerfell'
        },
        {
          first_name: 'Daenerys', 
          last_name: 'Targaryen',
          city: 'Dragonstone'
        },
        {
          first_name: 'Cersei', 
          last_name: 'Lannister',
          city: 'Westeros'
        }
      ])
    });
};
