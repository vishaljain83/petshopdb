
exports.seed = (knex, Promise) => {
  // Deletes ALL existing entries
  return knex('owner_pet_lks').del()
    .then(function () {
      // Inserts seed entries
      return knex('owner_pet_lks').insert([
        {
          owner_id: 1, 
          pet_id: 1
        },
        {
          owner_id: 2, 
          pet_id: 2
        },
        {
          owner_id: 3, 
          pet_id: 3
        }
      ]);
    });
};