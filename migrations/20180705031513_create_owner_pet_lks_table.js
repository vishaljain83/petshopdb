
exports.up = function(knex, Promise) {
    return knex.schema.createTable('owner_pet_lks', (table) => {
        table.increments('owner_pet_lks_id').primary()
        table.integer('owner_id')
        table.integer('pet_id')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists('owner_pet_lks')
};
