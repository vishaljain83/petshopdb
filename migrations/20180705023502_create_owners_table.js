
exports.up = (knex, Promise) => {
    return knex.schema.createTable('owners', (table) => {
        table.increments('owner_id').primary()
        table.string('first_name')
        table.string('last_name')
        table.string('city')
    })
};

exports.down = (knex, Promise) => {
    return knex.schema.dropTableIfExists('owners')
};
