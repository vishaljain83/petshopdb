
exports.up = (knex, Promise) => {
    return knex.schema.createTable('pets', (table) => {
        table.increments('pet_id').primary()
        table.string('name')
        table.date('birthday')
    })
};

exports.down = (knex, Promise) => {
    return knex.schema.dropTableIfExists('pets')
};
